﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
	// 패킷 타입
	public enum PacketType : short
	{ 
		IsDisconnected,			// 연결 종료됨

		CreateAccountRequest,	// 계정 생성 요청
		CreateAccountResponse,	// 계정 생성 응답

		LoginRequest,			// 로그인 요청
		LoginResponse,			// 로그인 응답
	}

	public static class Packet
	{
		/// <summary>
		/// 패킷 타입을 담는 바이트 배열 길이
		/// </summary>
		public const int PACKET_TYPE_SIZE = sizeof(PacketType);

		/// <summary>
		/// 헤더를 담는 바이트 배열 크기
		/// </summary>
		public const int HEADER_SIZE = sizeof(short);

		/// <summary>
		/// 패킷 타입을 수신합니다.
		/// </summary>
		/// <param name="socket">사용될 소켓 객체를 전달합니다.</param>
		/// <returns>수신된 패킷 타입을 반환합니다.</returns>
		public static async Task<PacketType> ReceiveAsyncPacketType(Socket socket)
		{
			// 패킷 타입을 받기 위한 버퍼를 생성합니다.
			byte[] packetTypeByte = new byte[PACKET_TYPE_SIZE];

			// 패킷 타입을 읽습니다.
			int readBytes = await socket.ReceiveAsync(packetTypeByte, SocketFlags.None);

			// 0 바이트를 읽은 경우
			if (readBytes < 1)
			{
				// 연결 종료됨
				return PacketType.IsDisconnected;
			}

			// 0 바이트가 아닌 경우
			// PACKET_TYPE_SIZE 보다 짧은 길이의 바이트를 읽은 경우
			// (아직 SocketType 에 대한 내용을 덜 읽은 경우)
			else if (readBytes < PACKET_TYPE_SIZE)
			{
				// 더 읽도록 합니다.
				await socket.ReceiveAsync(
					new ArraySegment<byte>(packetTypeByte, readBytes, PACKET_TYPE_SIZE - readBytes), 
					SocketFlags.None);
				/// ArraySegment(T[] array, int offset, int count)
				/// - 배열의 범위를 구분할 수 있도록 하기 위한 형식
				/// T : 배열의 형식을 전달합니다.
				/// array : 구분시킬 배열을 전달합니다.
				/// offset : 배열 범위의 시작 지점을 전달합니다.
				/// count : 배열 범위 시작 위치부터 끝 요소까지의 요소 개수를 전달합니다.
			}

			// 네트워크를 통해서 받은 패킷 타입을 얻습니다.
			PacketType packetType = (PacketType)IPAddress.NetworkToHostOrder(
				BitConverter.ToInt16(packetTypeByte));

			// 읽은 패킷 타입을 반환합니다.
			return packetType;
		}


		/// <summary>
		/// 전달한 Socket 객체를 통해 데이터를 보냅니다.
		/// </summary>
		/// <typeparam name="T">보낼 패킷 데이터 형식을 정의합니다.</typeparam>
		/// <param name="socket">데이터를 보내기 위하여 사용되는 Socket 객체를 전달합니다.</param>
		/// <param name="packetType">보낼 패킷 형태를 전달합니다.</param>
		/// <param name="data">보낼 데이터를 전달합니다.</param>
		/// <returns></returns>
		public static async Task SendAsync<T>(
			Socket socket,
			PacketType packetType,
			T data)
			where T : struct
		{
			// 패킷 타입을 Byte 배열로 변환합니다.
			byte[] packetTypeToBytes = BitConverter.GetBytes(
				IPAddress.HostToNetworkOrder((short)packetType));

			// 보낼 데이터를 Json 형태로 파싱합니다.
			string dataToJsonString = JsonConvert.SerializeObject(data);

			byte[] dataToBytes = Encoding.UTF8.GetBytes(dataToJsonString);

			// 헤더에 담을 데이터를 Byte 배열로 변환합니다.
			byte[] headerBytes = BitConverter.GetBytes(
				IPAddress.HostToNetworkOrder((short)dataToBytes.Length));

			// 데이터를 보내기 위한 바이트 버퍼를 생성합니다.
			byte[] bufferToSend = new byte[PACKET_TYPE_SIZE + HEADER_SIZE + dataToBytes.Length];



			// 바이트 배열에 담을 데이터의 시작 인덱스를 나타내기 위한 변수
			int offset = 0;

			// 패킷 타입을 버퍼에 기록합니다.
			Array.Copy(packetTypeToBytes, 0, bufferToSend, offset, PACKET_TYPE_SIZE);
			offset += PACKET_TYPE_SIZE;

			// 헤더를 버퍼에 기록합니다.
			Array.Copy(headerBytes, 0, bufferToSend, offset, HEADER_SIZE);
			offset += HEADER_SIZE;

			// 데이터를 버퍼에 기록합니다.
			Array.Copy(dataToBytes, 0, bufferToSend, offset, dataToBytes.Length);


			// 조합된 데이터를 보냅니다.
			await socket.SendAsync(bufferToSend, SocketFlags.None);
		}


		public static async Task<T> ReceiveAsyncHeaderAndData<T>(Socket socket) where T : struct
		{
			// 헤더를 읽습니다.
			byte[] headerByte = new byte[HEADER_SIZE];
			int readBytes = 0;
			while (readBytes < HEADER_SIZE)
			{
				int currentRead = await socket.ReceiveAsync(
					new ArraySegment<byte>(headerByte, readBytes, HEADER_SIZE - readBytes),
					SocketFlags.None);
				readBytes += currentRead;
			}

			// 헤더에 기록된 데이터 버퍼의 크기를 읽습니다.
			int dataBufferLength = IPAddress.NetworkToHostOrder(
				BitConverter.ToInt16(headerByte));

			// 데이터를 받기 위한 버퍼를 생성합니다.
			byte[] dataBytes = new byte[dataBufferLength];
			readBytes = 0;

			// 데이터를 모두 읽습니다.
			while (readBytes < dataBufferLength)
			{
				int currentRead = await socket.ReceiveAsync(
					new ArraySegment<byte>(dataBytes, readBytes, dataBufferLength - readBytes),
					SocketFlags.None);
				readBytes += currentRead;
			}

			// 받은 데이터를 Json 형태로 변환합니다.
			string bytesToJsonString = Encoding.UTF8.GetString(dataBytes);

			// Json 형식 문자열을 T 형식으로 변환합니다.
			T data = JsonConvert.DeserializeObject<T>(bytesToJsonString);

			// 읽은 데이터를 반환합니다.
			return data;
		}
	}
}
