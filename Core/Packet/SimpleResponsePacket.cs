﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    public struct SimpleResponsePacket
    {
        public bool result;
        public string context;

        public SimpleResponsePacket(bool result, string context = null)
        {
            this.result = result;
            this.context = context;
        }
    }
}
