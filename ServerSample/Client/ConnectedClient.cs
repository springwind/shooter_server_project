﻿using Core;
using MySql.Data.MySqlClient;
using MySqlX.XDevAPI.Relational;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServerSample.Client;

public class ConnectedClient
{
    /// <summary>
    /// 연결된 클라이언트 소켓 객체를 나타냅니다.
    /// </summary>
    public Socket clientSocket { get; private set; }

    /// <summary>
	/// 이 클라이언트의 연결이 종료되었을 경우 발생하는 이벤트
	/// </summary>
    public event Action<ConnectedClient> onDisconnected;

	/// <summary>
	/// 이 객체에서 서버쪽으로 쿼리문 요청시에 사용될 함수
	/// </summary>
	public event Action<string, Func<MySqlDataReader, Task>> requestQueryCmd;

	/// <summary>
	/// 공지된 내용이 존재하는 경우 발생하는 이벤트
	/// </summary>
	public event Func<ConnectedClient/*client*/, PacketType/*packetType*/, object/*context*/, Task/*retVal*/> onAnnounced;


	public ConnectedClient(Socket clientSocket)
    {
        this.clientSocket = clientSocket;

		Console.WriteLine("클라이언트 연결됨!" +
            $"[{clientSocket.RemoteEndPoint}]");

        // 클라이언트 데이터 수신 시작
        new Thread(DoListen).Start();
    }
	

    public async void DoListen()
    {
        try
        {
            while (true)
            {

				// 패킷을 받습니다.
				PacketType packetType = await Packet.ReceiveAsyncPacketType(clientSocket);

                // 연결 끊킴
                if(packetType == PacketType.IsDisconnected)
                {
                    onDisconnected?.Invoke(this);
                    break;
                }

                // 계정 생성 요청
				else if(packetType == PacketType.CreateAccountRequest)
                {
                    CreateAccountRequestPacket packet =
                        await Packet.ReceiveAsyncHeaderAndData<CreateAccountRequestPacket>(clientSocket);

                    Console.WriteLine("계정 생성 요청됨!");
                    Console.WriteLine($"id = {packet.id} / nickname = {packet.nickname}");

                    // 중복된 id / nickname 이 존재하는지 확인합니다.
                    requestQueryCmd(
                        $"SELECT * FROM {Constants.DB_SCHEMANAME}.{Constants.TABLE_ACCOUNTS} WHERE id = '{packet.id}' || nickname = '{packet.nickname}';",
                        async (table) =>
                        {
                            bool isDuplicated = false;
                            string context = null;

                            while(table.Read())
                            {
                                if (isDuplicated) break;

                                isDuplicated = true;

                                string readID = table["id"].ToString();
                                string readNickname = table["nickname"].ToString();

                                if (packet.id == readID)
                                    context = $"{readID} 는 사용할 수 없는 ID입니다.";
                                else if (packet.nickname == readNickname)
                                    context = $"{readNickname}은 사용할 수 없는 닉네임입니다.";

                                //Console.WriteLine($"id = {table["id"].ToString()}");
                                //Console.WriteLine($"pw = {table["pw"].ToString()}");
                                //Console.WriteLine($"nickname = {table["nickname"].ToString()}");

                            }

                            if(!isDuplicated)
                            {
                                // 요청된 내용을 table에 추가합니다.
                                requestQueryCmd(
                                    $"INSERT INTO {Constants.DB_SCHEMANAME}.{Constants.TABLE_ACCOUNTS} (id, pw, nickname) VALUES " +
                                    $"('{packet.id}', '{packet.pw}', '{packet.nickname}');", null);
                                Console.WriteLine($"{packet.id}계정 생성 성공.");
                            }
                            else Console.WriteLine($"{packet.id}계정 생성 실패.");

                            // 응답 결과를 생성합니다
                            SimpleResponsePacket responsePacket = new SimpleResponsePacket(
                                !isDuplicated,context);

                            // 응답 결과를 보냅니다.
                            await Packet.SendAsync(clientSocket,PacketType.CreateAccountRequest,responsePacket);

                        });
                        
                }

                // 로그인 요청
                else if(packetType == PacketType.LoginRequest)
                {
                    LoginRequestPacket packet = 
                        await Packet.ReceiveAsyncHeaderAndData<LoginRequestPacket>(clientSocket);

                    Console.WriteLine($"로그인 요청됨 {packet.id}");

                    bool finded = false;
                    requestQueryCmd(
                        $"SELECT * FROM {Constants.DB_SCHEMANAME}.{Constants.TABLE_ACCOUNTS} " +
                        $"WHERE id = '{packet.id}' && pw = '{packet.pw}';",
                        async(table) =>
                        {
                            string context = null;
                            while (table.Read()) finded = true;
                            if(!finded) context = $"ID와 PW를 확인하세요.";

                            // 응답 결과를 생성합니다.
                            SimpleResponsePacket responsePacket = new(finded, context);

                            //응답 결과를 보냅니다.
                            await Packet.SendAsync(clientSocket,
                                PacketType.LoginResponse,responsePacket);
                        });
                }
            }
        }
        catch (ObjectDisposedException)
        {
            // 클라이언트 연결 해제
            onDisconnected?.Invoke(this);
        }
        catch (Exception)
        {
            // 클라이언트 연결 해제
            onDisconnected?.Invoke(this);
        }
    }
}
