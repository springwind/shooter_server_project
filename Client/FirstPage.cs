using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientSample
{
	public partial class FirstPage : Form
	{
		private Socket _ConnectedServerSocket;

		public FirstPage()
		{
			InitializeComponent();

			Button_Connect.Click += OnConnectButtonClicked;

			//Label_Result.Text = "Hello World!";
		}

		public async Task ConnectAsync()
		{
			_ConnectedServerSocket = new(
				AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			IPEndPoint endPoint = new(
				IPAddress.Parse(Core.Constants.ADDRESS),
				Core.Constants.SERVERPORT);

			// 2초 대기
			//await Task.Delay(2000);

			// 연결시킵니다.
			await _ConnectedServerSocket.ConnectAsync(endPoint);

			Label_Result.Text = "연결되었습니다!";


			// 보낼 문자열
			string sendString = "Hello World!";

			// 문자열을 바이트 배열로 변환합니다.
			byte[] sendBytes = Encoding.UTF8.GetBytes(sendString);

			// 문자열을 보냅니다.
			await _ConnectedServerSocket.SendAsync(sendBytes, SocketFlags.None);
		}

		private async void OnConnectButtonClicked(object sender, EventArgs evt)
		{
			await ConnectAsync();
		}
	}
}
